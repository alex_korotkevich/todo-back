import { TPostData, TPostUpdateData } from "./post-types/post.types";
import { postDal } from "./post.dal";

class PostService {
  list = async (userId: number) => {
    try {
      const posts = await postDal.list(userId);
      return posts;
    } catch (err) {
      throw err;
    }
  };

  find = async (id: number | undefined, userId: number) => {
    try {
      if (!id) {
        throw new Error("Bad request");
      }
      const post = await postDal.find(id, userId);
      return post;
    } catch (err) {
      throw err;
    }
  };

  create = async (data: TPostData | undefined, userId: number) => {
    try {
      if (!data) {
        throw new Error("Bad request");
      }
      const post = await postDal.create(data, userId);
      return post;
    } catch (err) {
      throw err;
    }
  };

  remove = async (id: number | undefined, userId: number) => {
    try {
      if (!id) {
        throw new Error("Bad request");
      }
      await postDal.remove(id, userId);
    } catch (err) {
      throw err;
    }
  };

  update = async (
    id: number | undefined,
    data: TPostUpdateData | undefined,
    userId: number
  ) => {
    try {
      if (!id || !data) {
        throw new Error("Bad request");
      }
      await postDal.update(id, data, userId);
    } catch (err) {
      throw err;
    }
  };
}

export const postService = new PostService();
