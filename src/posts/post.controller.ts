import { Request, Response, Router } from "express";
import { authMiddleware, TAuthRequest } from "../middleware/auth.middleware";
import { TPostData, TPostUpdateData } from "./post-types/post.types";
import { postService } from "./post.service";

export const postRouter = Router();

postRouter.get(
  "/post",
  [authMiddleware],
  async (req: Request, res: Response) => {
    const userId = (req as TAuthRequest).userId;
    const posts = await postService.list(userId);
    res.send(posts);
  }
);

postRouter.get(
  "/post/:id",
  [authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = +req.params.id as number | undefined;
      const userId = (req as TAuthRequest).userId;
      const post = await postService.find(id, userId);
      res.send(post);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

postRouter.post(
  "/post",
  [authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const data = req.body as TPostData | undefined;
      const userId = (req as TAuthRequest).userId;
      const post = await postService.create(data, userId);
      res.send(post);
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

postRouter.delete(
  "/post/:id",
  [authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = +req.params.id as number | undefined;
      const userId = (req as TAuthRequest).userId;
      await postService.remove(id, userId);
      res.end();
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);

postRouter.put(
  "/post/:id",
  [authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const id = +req.params.id as number | undefined;
      const data = req.body as TPostUpdateData | undefined;
      const userId = (req as TAuthRequest).userId;
      await postService.update(id, data, userId);
      res.end();
    } catch (err) {
      res.status(400).send((err as Error).message);
    }
  }
);
