export type TPostData = {
  title: string;
  post: string;
};

export type TPostUpdateData = {
  post: string;
};
