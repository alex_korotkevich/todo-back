import { client } from "../pg-client";
import { TPostData, TPostUpdateData } from "./post-types/post.types";
const dataBaseName = "post";

class PostDal {
  list = async (userId: number) => {
    try {
      const res = await client(dataBaseName).select().where({ userId });
      return res;
    } catch (err) {
      throw new Error("DB Error");
    }
  };

  find = async (id: number, userId: number) => {
    try {
      const res = await client(dataBaseName)
        .select()
        .where({ id: id, userId: userId })
        .first();
      return res;
    } catch (err) {
      throw new Error("DB Error");
    }
  };

  create = async (data: TPostData, userId: number) => {
    try {
      const res = await client(dataBaseName)
        .insert({ title: data.title, post: data.post, userId: userId })
        .returning("*");
      return res[0];
    } catch (err) {
      throw new Error("DB Error");
    }
  };

  remove = async (id: number, userId: number) => {
    try {
      await client(dataBaseName).delete().where({ id, userId });
    } catch (err) {
      throw new Error("DB Error");
    }
  };

  update = async (id: number, data: TPostUpdateData, userId: number) => {
    try {
      await client(dataBaseName)
        .update({ post: data.post })
        .where({ id, userId });
    } catch (err) {
      throw new Error("DB Error");
    }
  };
}

export const postDal = new PostDal();
