import { client } from "../pg-client";

const tableName = "userToken";

class UserTokenDal {
  create = async (userId: number, token: string) => {
    await client(tableName).insert({ userId, token });
  };

  findOne = async (token: string) => {
    const tokenData = await client(tableName).select().where({ token }).first();
    return tokenData;
  };

  remove = async (userId: number) => {
    await client(tableName).del().where({ userId });
  };
}

export const userTokenDal = new UserTokenDal();
