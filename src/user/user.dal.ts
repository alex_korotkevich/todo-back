import { client } from "../pg-client";
const tableName = "user";

class UserDal {
  create = async (login: string, password: string) => {
    await client(tableName).insert({ login, password });
  };

  findByLogin = async (login: string) => {
    const user = await client(tableName).select().where({ login }).first();
    return user;
  };
}

export const userDal = new UserDal();
