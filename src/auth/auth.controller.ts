import { Request, Response, Router } from "express";
import { authMiddleware, TAuthRequest } from "../middleware/auth.middleware";
import { authService } from "./auth.service";

export const authRouter = Router();

authRouter.post("/registration", async (req: Request, res: Response) => {
  try {
    const login = req.body.login as string;
    const password = req.body.password as string;
    await authService.registration(login, password);
    res.end();
  } catch (err) {
    res.send((err as Error).message);
  }
});

authRouter.post("/login", async (req: Request, res: Response) => {
  try {
    const login = req.body.login as string;
    const password = req.body.password as string;
    const token = await authService.login(login, password);
    res.send(token);
  } catch (err) {
    res.send((err as Error).message);
  }
});

authRouter.get(
  "/logout",
  [authMiddleware],
  async (req: Request, res: Response) => {
    try {
      const userId = (req as TAuthRequest).userId;
      await authService.logout(userId);
      res.end();
    } catch (err) {
      res.send((err as Error).message);
    }
  }
);
