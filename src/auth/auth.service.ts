import { userTokenDal } from "../user/user-token.dal";
import { userDal } from "../user/user.dal";
import { generateToken } from "../util/generate-token";

class AuthService {
  registration = async (login: string, password: string) => {
    try {
      if (login.length < 5 || password.length < 5) {
        throw new Error("Login or password too short");
      }
      await userDal.create(login, password);
    } catch (err) {
      throw err;
    }
  };

  login = async (login: string, password: string) => {
    try {
      if (login.length < 5 || password.length < 5) {
        throw new Error("Login or password too short");
      }
      const user = await userDal.findByLogin(login);
      if (!user) {
        throw new Error("User not Found");
      }
      if (user.password !== password) {
        throw new Error("Password does not match");
      }

      const token = generateToken();
      await userTokenDal.create(user.id, token);
      return token;
    } catch (err) {
      throw err;
    }
  };

  logout = async (userId: number) => {
    await userTokenDal.remove(userId);
  };
}

export const authService = new AuthService();
