import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import { authRouter } from "./auth/auth.controller";
import { postRouter } from "./posts/post.controller";

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(authRouter);
app.use(postRouter);

app.listen(3005, () => {
  console.log("start server on 3005");
});
