import { knex } from "knex";

export const client = knex({
  client: "pg",
  connection: {
    user: "user",
    host: "localhost",
    database: "ToDo",
    password: "password",
    port: 5432,
  },
});
