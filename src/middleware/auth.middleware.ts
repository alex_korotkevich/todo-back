import { NextFunction, Request, Response } from "express";
import { userTokenDal } from "../user/user-token.dal";

export type TAuthRequest = Request & { userId: number };

export const authMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.headers.authorization;
  if (!token) {
    return res.status(403).send("Unauthorized");
  }
  const tokenData = await userTokenDal.findOne(token);
  if (!tokenData) {
    return res.status(403).send("Unauthorized");
  }
  (req as TAuthRequest).userId = tokenData.userId;
  next();
};
